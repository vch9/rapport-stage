\documentclass[11pt]{beamer}

\usetheme{montpellier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[outputdir=build]{minted}
%% \usepackage{array}
\usepackage[francais]{babel}

\title{Quick but Correct: Invariant-powered Property Testing}
\author{\textbf{Valentin Chaboche} <\url{valentinchb@gmail.com}>}
\subtitle{supervisé par Arvid Jakobsson et Zaynah Dargaye}
\institute{Université de Paris - Nomadic Labs}
\date{20 Septembre 2021}

\usepackage{lmodern}
\usepackage{cellspace}
\setlength{\cellspacebottomlimit}{5pt}
\setlength{\cellspacetoplimit}{5pt}
\setlength{\parindent}{0pt}

\newcommand\Wider[2][3em]{%
\makebox[\linewidth][c]{%
  \begin{minipage}{\dimexpr\textwidth+#1\relax}
  \raggedright#2
  \end{minipage}%
  }%
}

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\AtBeginSection[]
{
    \begin{frame}
        \frametitle{Plan}
        \tableofcontents[currentsection]
    \end{frame}
}

\addtobeamertemplate{footline}{\hfill\insertframenumber/\inserttotalframenumber}

\begin{document}
\maketitle

% ------------------------------------------------------------ %
% ------------------------- Contexte ------------------------- %
% ------------------------------------------------------------ %

\section{Contexte}

\begin{frame}{Contexte}
  \begin{columns}[c]
    \column{2.5in}
    \begin{itemize}
    \item Tezos
    \end{itemize}
    Blockchain distribuée, support des smart contracts, auto-amendement, etc.

    \vspace{5mm}
    
    \begin{itemize}
    \item Nomadic Labs
    \end{itemize}
    Centre de recherche et développement de plus de 50 ingénieurs.
    Financé par la fondation Tezos.
    \column{2in}
    \includegraphics[scale=0.35]{images/tezos.png}

    \includegraphics[scale=0.13]{images/nomadiclabs.png}
  \end{columns}
\end{frame}


% ------------------------------------------------------------ %
% ----------------------- Processus MR ----------------------- %
% ------------------------------------------------------------ %

\section{Introduction}

\begin{frame}{Processus de développement par \emph{merge request}}
  Processus de développement sur Tezos:
  \begin{itemize}
  \item Proposition de modifications via des \emph{merge requests}.
  \item Chacune nécessite 2 approbations par la \textit{merge team}.
  \item $\simeq$ 75 merge requests acceptées par mois.
  \item $\simeq$ 23 jours pour intégrer la merge request.
  \end{itemize}
  
  \pause

  Différentes manières d'aider à la validation une merge request:
  \begin{itemize}
  \item{Test unitaire}
  \item{Vérification formelle du code}
  \item{Property-based testing (PBT)}
  \end{itemize}
\end{frame}

\begin{frame}{Property-based testing}
  PBT = fonction à tester + propriété à valider + générateur

  \vspace{3mm}
  \begin{itemize}
  \item Généralisation des tests unitaires pour un plus grand domaine testé.
  \item Pas de biais grâce à la génération aléatoire des entrées.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Type et générateur d'arbre}
  \small
  \begin{minted}{ocaml}
type tree = Leaf of int | Node of tree * tree

let gen_tree : tree QCheck2.Gen.t =
  let open QCheck2.Gen in
  sized @@ fix (fun self -> function
    | 0 -> map (fun x -> Leaf x) nat
    | n ->
      oneof [
        map (fun x -> Leaf x) nat;
        map2 (fun x y -> Node (x, y))
          (self (n / 2)) (self (n / 2));
      ])
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Tester l'involution du renversement d'un arbre}
  \small
  \begin{minted}{ocaml}
type tree = Leaf of int | Node of tree * tree

(* Générateur *)
let gen_tree : tree QCheck2.Gen.t = (* ... *)

(* Fonction à tester *)
let rec rev = function
  | Leaf x -> Leaf x
  | Node (left, right) -> Node (rev right, rev left)

let test =
  QCheck2.Test.make ~name:"rev (rev tree) = tree" ~count:1000
  gen_tree
    (* Propriété à valider *)
    (fun tree -> rev (rev tree) = tree)
  \end{minted}
  
  \blfootnote{involutive : \texttt{$\forall$ f x, f (f x) = x}}
\end{frame}

\begin{frame}{Les limites du property-based testing}
  Quels défauts peut-on relever sur cette méthode de test ?
  \vspace{5mm}
  
  \begin{itemize}
  \item Nécessite du code administratif pour écrire des générateurs.
  \item Répétition des propriétés.
  \item Nécessite une expertise du code pour extraire des propriétés.
  \end{itemize}
\end{frame}

\begin{frame}{Les solutions que j'ai apportées}
  \begin{itemize}
  \item \texttt{ppx\_deriving\_qcheck}: \\ génération automatique de générateurs de donnée.
    \vspace{3mm}
  \item \texttt{ppx\_pbt}: \\ système d'annotation à base de propriété et génération de test.
    \vspace{3mm}
  \item \texttt{osnap}: \\ méthode automatique pour effectuer des tests de régression.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\texttt{ppx\_deriving\_qcheck}}
  Utilisation de la méta-programmation avec un PPX pour dériver des générateurs
  depuis une déclaration de type:
  \vspace{5mm}

  \begin{minted}{ocaml}
type tree = Leaf of int | Node of tree * tree
[@@deriving qcheck]
  \end{minted}
\end{frame}

\begin{frame}[fragile]{\texttt{ppx\_pbt}}
  \begin{itemize}
  \item Langage de spécification pour les propriétés répétitives.
  \item Génération de test basée sur un interpréteur inclut dans un PPX.
  \end{itemize}

  \vspace {5mm}
  
  \begin{minted}{ocaml}
 let rec rev = function
   | Leaf x -> Leaf x
   | Node (left, right) -> Node (rev right, rev left)
 [@@pbt "involutive[gen_tree]"]
  \end{minted}

  \blfootnote{involutive : \texttt{$\forall$ f x, f (f x) = x}}
\end{frame}

\section{osnap: méthode automatique pour effectuer des tests de régression}

\begin{frame}[fragile]{\texttt{ppx\_expect}: comparer textuellement des résultats}
  \begin{minted}{ocaml}
  let%expect_test "addition" =
    printf "%d" (1 + 2);
    [%expect {| 4 |}]
  \end{minted}
  \vspace{3mm}

  \pause

  ppx\_expect effectue une différence textuel sur le résultat:
  \vspace{3mm}

  \begin{minted}{text}
  let%expect_test "addition" =
    printf "%d" (1 + 2);
-|  [%expect {| 4 |}]
+|  [%expect {| 3 |}]
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Etendre avec la génération aléatoire de scénario}
  Notre objectif est de généraliser ce concept avec de la génération aléatoire:
  \vspace{3mm}
  \begin{itemize}
  \item Scénarios aléatoires plutôt qu'un test unitaire.
  \item Enregistrement des scénarios pour créer des tests de régression.
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]{Spécifier une fonction pour générer des scénarios}
  \small
  \begin{minted}{ocaml}
(** ['a spec] combine un générateur et optionnellement un
    afficheur *)
type 'a spec = { gen : 'a gen; printer : 'a printer option }

(** [t] est le type de spécification d'une fonction, il décrit
    comment générer les paramètres et, au minimum, comment
    afficher le résultat *)
type ('fn, 'r) t =
  | Result : 'r printer -> ('r, 'r) t
  | Arrow : 'a spec * ('fn, 'r) t -> ('a -> 'fn, 'r) t

val ( ^>> ) : 'a spec -> 'b printer -> ('a -> 'b, 'b) t
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Spécifier une fonction via des combinateurs}
  \small
  \begin{minted}{ocaml}
open Osnap

let rec sum = function
  | Leaf x -> x
  | Node (left, right) -> sum left + sum right

let spec_tree : tree Spec.spec = Spec.of_gen gen_tree
  
let spec_sum : (tree -> int, int) Spec.t =
  Spec.(spec_tree ^>> string_of_int)
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Générer des scénarios avec un runner de test}
  \small
  \begin{minted}{ocaml}  
open Osnap

let test =
  Test.make ~path:".osnap/sum" ~count:4
            ~name:"sum"        ~spec:spec_sum
             sum
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Exemple des scénarios automatiquement générés}
  En utilisant la spécification et le test, nous avons suffisament d'informations
  pour générer des scénarios:
  \vspace{2mm}
  
  \begin{minted}{text}
          .               |             .
sum      / \      10      |    sum     / \      0
        5   5             |           0   0
                          |
          .               |              .
         / \              |             / \
sum     6   .     7       |    sum     .   3    6
           / \            |           / \
          0   1           |          1   2
  \end{minted}
\end{frame}


\begin{frame}[fragile]{Utiliser ces tests sur des nouvelles versions}
  \small
  \begin{minted}{ocaml}
let sum tree =
  let rec sum tree cont = match tree with
    | Leaf x -> cont x
    | Node (left, right) ->
      sum left (fun sum_left ->
        sum right (fun sum_right ->
          cont (sum_left + sum_right)))
  in sum tree (fun x -> x)

open Osnap
  
let test =
  Test.make ~path:".osnap/sum" ~count:4
            ~name:"sum"        ~spec:spec_sum
            sum
  \end{minted}
\end{frame}

\begin{frame}{Schéma d'utilisation}
  \Wider{\includegraphics[width=12cm, height=4.5cm]{"images/osnap.drawio.png"}}
\end{frame}

\section{Conclusion}

\begin{frame}{Récapitulatif des solutions apportées}
  \begin{itemize}
  \item Problème: code administratif pour écrire des générateurs
    \\ Solution:  \texttt{ppx\_deriving\_qcheck}
  \item Problème: répétition des propriétés
    \\ Solution: \texttt{ppx\_pbt}
  \item Problème: expertise pour extraire les propriétés
    \\ Solution: \texttt{osnap}
  \end{itemize}
\end{frame}

\begin{frame}{Récapitulatif des outils développés}
  \footnotesize
  \Wider[4em]{
  \begin{tabular}{|*5{Sc|}}
    \hline
                                   & Testé & Licence & Temps consacré & Lignes de code\footnotemark[1]\\
    \hline
    \texttt{ppx\_deriving\_qcheck} & Oui   & MIT     & 1.5 mois       & 603 \\
    \hline
    \texttt{ppx\_pbt}              & Oui   & MIT     & 2.5 mois       & 1681  \\
    \hline
    \texttt{osnap}                 & Oui   & MIT     & 1.5 mois       & 1207 \\
    \hline
  \end{tabular}}

  \footnotetext[1]{Sans compter les tests}
\end{frame}

\begin{frame}
  \center{Merci pour votre attention !}
\end{frame}

\section{Annexes}

\begin{frame}[fragile]{Code généré par ppx\_pbt pour tester l'addition}
  \begin{minted}{ocaml}
let rec rev = function
   | Leaf x -> Leaf x
   | Node (left, right) -> Node (rev right, rev left)
 [@@pbt "involutive[gen_tree]"]

(* généré par ppx_pbt ==> *)

let test_rev_is_involutive =
  QCheck2.Test.make ~name:"rev_is_involutive"
    gen_tree
    (fun x -> rev (rev x) = x)

let () = Runner.add_tests [ test_rev_is_involutive ]
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Code généré par ppx\_deriving\_qcheck sur un arbre}
  \small
  \begin{minted}{ocaml}
type tree = Leaf of int | Node of tree * tree
[@@deriving qcheck]

(* généré par ppx_deriving_qcheck ==> *)

let gen_tree =
  let open QCheck2.Gen in
  sized @@ fix @@
    fun self -> function
    | 0 -> map (fun gen0 -> Leaf gen0) int)
    | n ->
      frequency
      [ (1, (map (fun gen0 -> Leaf gen0) int));
        (1, (map (fun (gen0, gen1) -> Node (gen0, gen1))
              (pair (self (n / 2)) (self (n / 2)))))]))
  \end{minted}
\end{frame}

\end{document}
